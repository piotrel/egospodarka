import scrapy
import re

class EGospodarkaSpider(scrapy.Spider):
    name = "egospodarka"
    start_urls = [
        "http://firma.egospodarka.pl/raporty-o-firmach/k,18839.budownictwo.html",
        "http://firma.egospodarka.pl/raporty-o-firmach/k,18848.transport.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,16973.roznorodny-handel-detaliczny.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,78.sklepy-odziezowe.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,17124.sklepy-spozywcze.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,79.sklepy-z-meblami-artykulami-wyposazenia-domu-i-sprzetem-gospodarstwa-domowego.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,17123.sklepy-zaopatrzenia-ogolnego.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,17122.sprzedaz-detaliczna-materialow-budowlanych-artykulow-metalowych-i-ogrodniczych-oraz-przyczep-mieszkalnych.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,17121.sprzedaz-hurtowa-artykulow-nietrwalych.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,17120.sprzedaz-hurtowa-artykulow-trwalych.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,12.naprawa-pojazdow-samochodowych-i-inne-uslugi.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,10.uslugi-dla-ludnosci.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,17119.uslugi-elektryczne-gazownicze-i-sanitarne.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,17.uslugi-i-porady-prawne.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,22.uslugi-inzynieryjne-i-menadzerskie.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,13.uslugi-naprawcze-i-serwisowe.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,15.uslugi-rekreacyjne-i-rozrywkowe-z-wyjatkiem-filmowych.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,11.uslugi-zwiazane-z-dzialalnoscia-gospodarcza.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,17080.uslugi-zwiazane-z-rolnictwem.html",
        "http://www.firma.egospodarka.pl/raporty-o-firmach/k,17089.wykonawstwo-robot-specjalistycznych.html"
    ]

    def parse(self, response):
        prog = re.compile(r'(\d+)(\.)(\D+)(\d+)?(\.)')
        kategoriaMatch = prog.search(response.request.url)
        kategoriaDirty = kategoriaMatch.group(3)
        kategoria = kategoriaDirty.replace('-strona-', '')
        
        for href in response.xpath('//a[@class="ico-tel"]/@href'):
            yield response.follow(href, callback=self.company_parse, meta={'kategoria': kategoria})
        for href in response.xpath('//p[@class="art-pag-3"]/a/@href'):
            yield response.follow(href, callback=self.parse)

    def company_parse(self, response):
        prog = re.compile(r'\d{2}\s\d{2}\s')
        telefony = response.xpath('//a[contains(@href, "kto-dzwonil")]/text()').extract()
        telefon = telefony[0] if prog.match(telefony[0]) is None else ""
        if (len(telefony) > 1):
            telefon = telefony[1] if prog.match(telefony[1]) is None else ""

        if telefon is not "":
            yield {
                'nazwa': response.xpath('//div[@class="firma-dane"]/h1/text()').extract_first(),
                'telefon': telefon,
                'województwo': response.xpath('//div[@class="ego-path"]/a[4]/text()').extract_first(),
                'kategoria': response.meta.get('kategoria')
            }